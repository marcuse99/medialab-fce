# medialab-fce


## Descripción

Repositorio GIT para investigación y desarrollo del entorno virtual del Medialab FCE.

**Estado actual:** Etapa 1 Diagnóstico

## Visión general

Durante el encuentro [Futuribles](https://futuribles.eco.unc.edu.ar/) la comunidad docente planteó una serie de dificultades recurrentes para conectar con la comunidad estudiantil, captar su atención y participación. La centralidad de esta necesidad plantea un desafío importante en nuestra tarea de acompañar los procesos de enseñanza y aprendizaje.
Entendiendo que sin comunicación no hay educación posible, desatender estas modificaciones culturales desde la universidad implica un serio problema. La nueva cultura digital ha modificado las jerarquías y direccionalidades de las formas de comunicación analógicas. En términos esquemáticos, ya no se trata de emisores y receptores, ni de emisores y masas de receptores, sino de participación directa y descentralizada de consumidores-prosumidores. Las formas de lectura y escritura también se han trastocado, generando nuevas configuraciones en los tiempos, los espacios, las dinámicas de participación y el imaginario. Estamos frente a una nueva transformación del sensorium, es decir, de las formas de percibir y actuar en el mundo.

Las cátedras que han detectado esto, buscan adaptarse a partir de prueba y error. Han habido casos de éxito, fracasos y situaciones que plantean nuevos conflictos; pero lo que se ha descubierto compartiendo las experiencias en Futuribles es que no hay una receta genérica: cada campo disciplinar, cada cátedra, tiene sus especificidades, limitaciones y oportunidades.

En este marco, los esfuerzos desde el espacio multimedia de FyPE se enfocaron hasta el momento en colaborar en la producción de contenidos, luego, viendo que la magnitud de la demanda de producción era inalcanzable, nos enfocamos en propuestas de formación docente en herramientas y lenguajes audiovisuales. En estas primeras etapas comenzamos a equipar un estudio de grabación/edición multimedia. Es decir, nuestra destinataria exclusiva fue la comunidad docente y el enfoque estuvo puesto específicamente en la apropiación y formación tecnológica. Pero la necesidad planteada en Futuribles no pasa por producir videos o aprender a usar plataformas de videoconferencia o streaming, sino en una cuestión más fundamental: lograr conectar con la comunidad estudiantil.

Por eso, adaptarnos a este nuevo escenario nos implica reorientar nuestro objetivo. Ya no se trata de producir materiales audiovisuales o alfabetizar en tecnologías puntuales, se trata de introducirnos en el nuevo sensorium junto a la comunidad estudiantil, propiciar nuevas y efectivas formas de acercarnos, mediar, conectar, entusiasmar, dinamizar la comunicación en los procesos de enseñanza y aprendizaje. En esta línea es que se inscribe la propuesta de creación de un Media Lab en la Facultad.

Un Media Lab (laboratorio de medios) es un espacio dedicado a la investigación, producción y difusión en relación a técnicas y artes mediales. Son espacios caracterizados por la transdisciplinariedad y la innovación, que suelen presentar aportes disruptivos tanto en el campo académico como en el productivo.

Existen distintos Media Lab con improntas diferentes según su entorno y objetivos. El espacio pionero fue el [Media Lab del MIT](https://www.media.mit.edu/) fundado en 1985 y dedicado a proyectos de investigación y desarrollo en la convergencia de diseño, multimedia y tecnología. De este espacio surgieron innovaciones importantes como la pantalla táctil, el GPS para automóviles, la tinta electrónica, el software para aprendizaje de código Scratch, juegos con nuevos dispositivos como Guitar Hero y más.

Una variante que demuestra la versatilidad de estos proyectos es el [Media Lab Madrid](https://www.medialab-matadero.es/) (luego llamado Prado y actualmente Matadero), creado en el 2002 por el ayuntamiento de Madrid como un espacio dedicado a la cultura digital y a la producción de proyectos de carácter multidisciplinar. Este espacio presentó muy buenos resultados en la participación ciudadana y fue creciendo y adaptándose hasta el día de hoy, más enfocado en la producción de artes visuales digitales y la impresión 3D.

Otro caso de Media Lab, ya en contextos de educación superior, es el [Medialab Uniovi](https://medialab-uniovi.es/) fundado en 2018 en la Universidad de Oviedo, España. En este caso se presenta como una cátedra universitaria donde “las líneas de trabajo buscan dar valor a los proyectos para que tengan una aplicación práctica en el mercado y la sociedad así como la creación de empresas o start-ups y/o la realización de publicaciones. Cada línea de trabajo tiene un profesor de la Universidad de Oviedo como responsable y, a su vez, cada una de las líneas consta de proyectos más específicos donde los estudiantes pueden llevar a cabo su TFG, TFM o Tesis doctoral.” (Wikipedia).

A partir de estos antecedentes y proyectando un diagnóstico propio de nuestra Facultad buscamos generar un espacio de encuentro y co-creación entre la comunidad estudiantil, docente y técnica de la Facultad, que propicie lógicas participativas, en red, multi y transdisciplinares, que facilite procesos de innovación y de fortalecimiento de la inteligencia colectiva en nuestra Facultad.

___
## Objetivo general

- Fortalecer los procesos de enseñanza y aprendizaje en la Facultad de Ciencias Económicas a partir del trabajo transdisciplinar en relación a tecnologías y artes mediales.

## Objetivos específicos

- Promover una comunidad transdisciplinar y horizontal de enseñanza y aprendizaje en relación a las tecnologías y artes mediales.
- Investigar las dinámicas que pueden aportar las técnicas y artes mediales a los procesos de enseñanza y aprendizaje de cada cátedra.
- Aportar a la producción de contenido educativo multimedia.
- Consolidar un espacio físico para el encuentro y producción en torno a las artes mediales.
- Realizar eventos abiertos e intervenciones artísticas en el espacio físico de la universidad.
- Difundir los conocimientos generados.

___
## Hoja de ruta

### 1. Diagnóstico

Duración: 2 meses (Abril-Mayo)

- [] Análisis de registros de Futuribles
- [] Investigación del estado del arte
- [] Entrevistas en profundidad con agentes clave (FyPE, ARI, Área de Sistemas)
- [] Focus groups (docentes, estudiantes, no-docentes, equipos de investigación)

### 2. Diseño
Duración: 2 meses (Junio-Julio)
- [] Diseño de metodología de funcionamiento
- [] Armado y organización del equipo de mediación
- [] Prototipado de líneas de trabajo y convocatoria a proyectos piloto
- [] Montaje del espacio físico y virtual
- [] Producción de piezas comunicacionales para etapa piloto.

### 3. Piloto
Duración: 5 meses (Agosto - Diciembre)
- [] Puesta en marcha del espacio a partir de un grupo reducido de proyectos.
- [] Presentación/comunicación de los avances de los proyectos piloto.
- [] Análisis de los casos. Reajuste de la dinámica.

### 4. Lanzamiento
Marzo 2023
- [] Comunicación del espacio, convocatoria a proyectos/asistentes
- [] Inauguración
___
## Colaborar

El Medialab es un proyecto abierto a la colaboración, especialmente de la comunidad docente, estudiantil y no-docente de la Universidad Nacional de Córdoba.

- Colaborar en este repositorio
- Colaborar en los proyectos específicos
- Colaborar en el seguimiento de proyectos

## Autoría
[Área de Formación Docente y Producción Educativa FCE UNC](https://www.eco.unc.edu.ar/fype)

## Licencia
Creative Commons BY SA 4.0