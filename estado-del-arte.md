# Estado del arte

Lista de enlaces y lecturas para el desarrollo del medialab.

## Lecturas y bibliografía
- [Los cuatro ejes de la cultura participativa actual. De las plataformas virtuales al medialab](https://icono14.net/ojs/index.php/icono14/article/view/904)
- [The Media Lab: Inventing the Future at M.I.T.](https://aip.scitation.org/doi/pdf/10.1063/1.4822669)
- [Laboratorios sociales en Universidades : Innovación e impacto en Medialab UGR](https://www.torrossa.com/en/resources/an/4131099)
- [El modelo del medialab](https://www.academia.edu/34135115/El_modelo_del_MediaLab_1_)
- [MediaLab Universidad Panamericana: Una experiencia heutagógica en la formación en medios de comunicación durante el confinamiento por Covid19](https://www.academia.edu/49614295/MediaLab_Universidad_Panamericana_A_heutagogical_experience_in_media_training_during_Covid19_confinement_MediaLab_Universidad_Panamericana_Una_experiencia_heutag%C3%B3gica_en_la_formaci%C3%B3n_en_medios_de_comunicaci%C3%B3n_durante_el_confinamiento_por_Covid19?sm=b)
- [Social Medialab. Tecnología e ideología en los proyectos de intervención sociocomunitaria en el ámbito universitario](https://www.academia.edu/75433188/Social_Medialab_Tecnolog%C3%ADa_e_ideolog%C3%ADa_en_los_proyectos_de_intervenci%C3%B3n_sociocomunitaria_en_el_%C3%A1mbito_universitario?sm=b)
- [De la Bauhaus a los MediaLab: una síntesis apretada de 100 años de evolución](https://www.academia.edu/39031171/De_la_Bauhaus_a_los_MediaLab_una_s%C3%ADntesis_apretada_de_100_a%C3%B1os_de_evoluci%C3%B3n?sm=b)
- [El Aula-Medialab: un espacio universitario de experimentación y transformación social](https://www.academia.edu/45246729/El_Aula_Medialab_un_espacio_universitario_de_experimentaci%C3%B3n_y_transformaci%C3%B3n_social)
- [El modelo medialab](https://www.academia.edu/34135115/El_modelo_del_MediaLab_1_)
- [New Affective Models of Knowledge Transmission: The Medialab- Prado as a Trading Zone](https://www.academia.edu/31355959/New_Affective_Models_of_Knowledge_Transmission_The_Medialab_Prado_as_a_Trading_Zone)
- [Otros resultados de búsqueda en Academia.edu](https://www.academia.edu/search?page=3&q=medialab)
- [Resultados de búsqueda en repositorio MinCyT](https://repositoriosdigitales.mincyt.gob.ar/vufind/Search/Results?lookfor=medialab&type=AllFields)

## Proyectos interesantes

- [Un universo en el que no nos extinguimos](https://odoediciones.mx/alargerreality/)
